package north.com.multiprocessinstance.camunda.task;

import north.com.multiprocessinstance.entity.CreditLoaner;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.Arrays;
import java.util.List;

public class LoadUsersTask implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        List<CreditLoaner> users = Arrays.asList(new CreditLoaner("Sarah", "Cox"), new CreditLoaner("John", "Doe"), new CreditLoaner("Filip", "Trajkovski"));
        delegateExecution.setVariable("userList", users);
    }
}
