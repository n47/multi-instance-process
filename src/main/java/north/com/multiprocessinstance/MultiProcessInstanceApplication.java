package north.com.multiprocessinstance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiProcessInstanceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiProcessInstanceApplication.class, args);
    }

}
