package north.com.multiprocessinstance.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import north.com.multiprocessinstance.controller.dto.MessageEventRequest;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProcessService {

    private final ActiveCamundaSupport activeCamundaSupport;

    public void sendMessageEvent(String processInstanceId, MessageEventRequest messageEventRequest) {
        Map<String, Object> correlationKeys = new HashMap<>();
        messageEventRequest.getCorrelationKeys().forEach(correlationKeyDto -> correlationKeys.put(correlationKeyDto.getKey(), correlationKeyDto.getValue()));
        activeCamundaSupport.createMessageCorrelationWithCorrelationKeys(processInstanceId, messageEventRequest.getMessageName(), correlationKeys);
    }

}
