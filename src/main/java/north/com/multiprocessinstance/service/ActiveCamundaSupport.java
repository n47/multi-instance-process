package north.com.multiprocessinstance.service;

import org.camunda.bpm.engine.ProcessEngines;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ActiveCamundaSupport {

    public void createMessageCorrelationWithCorrelationKeys(String processInstanceId, String messageName, Map<String, Object> correlationKeys) {
        ProcessEngines.getDefaultProcessEngine().getRuntimeService()
                .createMessageCorrelation(messageName)
                .localVariablesEqual(correlationKeys)
                .processInstanceId(processInstanceId)
                .correlate();
    }
}
