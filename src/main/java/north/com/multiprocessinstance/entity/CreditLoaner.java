package north.com.multiprocessinstance.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class CreditLoaner {

    private String firstName;
    private String lastName;
    private Boolean registered;
    private String email;

    public CreditLoaner(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
