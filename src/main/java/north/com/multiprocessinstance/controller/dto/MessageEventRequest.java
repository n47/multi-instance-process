package north.com.multiprocessinstance.controller.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class MessageEventRequest {

    private String messageName;
    private List<CorrelationKeyDto> correlationKeys;
}
