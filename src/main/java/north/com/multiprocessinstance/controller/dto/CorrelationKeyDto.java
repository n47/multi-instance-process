package north.com.multiprocessinstance.controller.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CorrelationKeyDto {

    private String key;
    private String value;
}
