package north.com.multiprocessinstance.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import north.com.multiprocessinstance.controller.dto.MessageEventRequest;
import north.com.multiprocessinstance.service.ProcessService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@Slf4j
@RestController
public class ProcessController {

    private final ProcessService processService;

    @PostMapping(value = "/process/{processInstanceId}/messageEvent")
    public ResponseEntity<Void> sendMessageEvent(@RequestBody MessageEventRequest messageEventRequest, @PathVariable String processInstanceId) {
        log.info("Received request to send message event");
        processService.sendMessageEvent(processInstanceId, messageEventRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
